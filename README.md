# DSSG 2021 - ADFC bike counter data

This repository was started during the DSSG Berlin datathon 2021. 

## Data

- bike counters that were previously provided (data was scraped from eco-counter, for instance [this station](https://data.eco-counter.com/public2/?id=100064714))
- examplary weather data
- potential additional data sources:
  - [FixMyBerlin](https://fixmyberlin.de/zustand)

# Credit
- Icon: Photo by [Nuno Ricardo](https://unsplash.com/@nunoricardo) on [Unsplash](https://unsplash.com/s/photos/bike)
  
